//hope you like long files
var i = 0;
var j = 0;
var r = 0;
var s = 0;
var colorArray = ["rgba(255,182,182,.5)","rgba(255,219,102,.5)","rgba(255,249,132,.5)","rgba(141,255,91,.5)","rgba(135,255,240,.5)","rgba(80,255,139,.5)","rgba(43,135,255,.5)"];
var pager = new Pager(100); 
var allQuest = [];
var qSet = false;
var numbAnsChoices = [];
var secSet = false; 

///////////// FOR JSON STUFF ///////////////////
var QM134b_TestQ = {
	qNumber:null,
	qType: null,
	qWorth:null,
	qText:"",
	qAnswers:null,
	qCorrectAnswer:null
};

function createQuestionFrame(){
	QM134b_TestQ.qNumber = i;
	QM134b_TestQ.qWorth = document.getElementById("p"+j).value;
	QM134b_TestQ.qText = document.getElementById("question"+j).value;
	QM134b_Test.testQuestions.push(QM134b_TestQ);
}
///////////// FOR JSON STUFF ///////////////////

//drop function
function allowDrop(ev)
{
ev.preventDefault();
}

//drag function
function drag(ev)
{
ev.dataTransfer.setData("Text",ev.target.id);
}


//create a drop function{ 
	/*
	-get location where to start the question
	-have some type of switch case that determines which question is being dragged{ 
		call the correct question post with the param location as the post param 
	}
	*/
function drop(ev)
{

	ev.preventDefault();

	// i can get decremented, j will not (for unique id's)
	i += 1;
	j += 1;
	//do a check for slider
	if($("#button").html() === "Maximize"){
			$("#button").html("Minimize");
			$('.small').show();
			$('#pool').show();
			$(".cont").slideToggle();
		}
	var imgName=ev.dataTransfer.getData("Text");
	var data=document.createElement("div");
	data.setAttribute("id","q"+j);
	data.setAttribute("class","custQ");
	//create all the questions using innerhtml
		if(imgName==="TFCreate"){
			data.setAttribute("data-qtype","TF");
			data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'> <label for = 'p"+j+"'>Question Worth </label> <input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'> </div> <div class = 'cont'>  <br> <br> <h4 > True / False Question </h4> <p> <b>  Insert Question Below: </b> </p>  <input type = 'text'  class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'  > <br> <p> <b> Insert Answer Below: </b> </p> <input type = 'radio' name = 'ans"+j+"' id = 'ans"+j+"-1' value='True'/>True <br> <input type = 'radio' name = 'ans"+j+"' id = 'ans"+j+"-2' value='False'/> False <br> <img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'> <br> </div><hr>";
			numbAnsChoices.push(2);
		}

		if( imgName==="MCOCreate"){
			data.setAttribute("data-qtype","MCO");
			data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label><input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'>	</div><div class = 'cont'> <br><br><h4 >  Multiple Choice (One Option) Question </h4><p> <b>  Insert Question Below: </b> </p> <input type = 'text'  class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'   ><br><p> <b> Insert Answer Below: </b> </p><input type='radio' name = 'ans"+j+"' id = 'ans"+j+"-1'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-1' ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'>	<br><input type='radio' name = 'ans"+j+"' id = 'ans"+j+"-2'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-2' ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'>	<br><input type='radio' name = 'ans"+j+"' id = 'ans"+j+"-3'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-3'  >	<img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><button type='button' class= 'addButton'>Add More Answers</button> <img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'><br>	</div> <hr>";
			numbAnsChoices.push(3);
		}

		if(imgName==="MCACreate"){
			data.setAttribute("data-qtype","MCA");
			data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label>	<input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'>	</div><div class = 'cont'> <br><br><h4>  Multiple Choice (All Options) Question </h4> <p> <b>  Insert Question Below: </b> </p><input type = 'text' class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'  ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><p> <b> Insert Answer Below: </b> </p>	<input type='checkbox' name = 'ans"+j+"-1' id = 'ans"+j+"-1'/><input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-1'  ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><input type='checkbox' name = 'ans"+j+"' id = 'ans"+j+"-2'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-2'  ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><input type='checkbox' name = 'ans"+j+"' id = 'ans"+j+"-3'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-3'  ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><button type='button' class= 'addButton'>Add More Answers</button>	<img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'><br></div><hr>";
			numbAnsChoices.push(3);
		}
		if(imgName==="FITWECreate"){
			data.setAttribute("data-qtype","FITWE");
			data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label><input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'>	</div>	<div class = 'cont'><br>	<br><h4>  Fill In the Blank - End of Sentence Question </h4> <p> <b>  Insert  Question Below: </b> </p>  <input type = 'text' class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'  ><br><p> <b> Insert Answer Below: </b> </p><input type='text' id='ans"+j+"-1' class='Questions_FITB' autocomplete='off' /> <br><img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'><br></div><hr>";
			numbAnsChoices.push(1);
		}
		if(imgName==="FITWMCreate"){
			data.setAttribute("data-qtype","FITWM");
			data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label>	<input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'></div><div class = 'cont'><br><br><h4>  Fill In the Blank - Middle of Sentence Question </h4> <p> <b>  Insert Beginning of Question Below: </b> </p> <input type = 'text'  class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'><br><p> <b> Insert Answer Below: </b> </p><input type='text' id='ans"+j+"-1' class='Questions_FITB' autocomplete='off' /> <br> <p> <b>  Insert End of Question Below: </b> </p> <input type = 'text'  class = 'questionBlanked' id='ans"+j+"-2' > <br><img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'> <br></div><hr> ";
			numbAnsChoices.push(2);
		}
		if(imgName==="SACreate"){
			data.setAttribute("data-qtype","SA");
			data.innerHTML = "<h3> #"+s+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label><input type = 'text' name = 'p"+j+"' id = 'p"+j+"' class = 'worth'></div><div class = 'cont'><br><br><h4>  Short Answer Question </h4><p> <b>  Insert Question Below: </b> </p><textarea id = 'question"+j+ "' class = 'questionBlanked' name = 'question"+j+ "' rows = '3'  ></textarea><br><img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'><br></div><hr>";
			numbAnsChoices.push(0);
		}
		if(imgName==="SeCreate"){
			data.setAttribute("data-qtype","Se");
			r++;
			s++;
			i--;
			j--;
			data.innerHTML = "<h3> Section #" +r + "<span class = 'qTitle'>	</span></h3><div class = 'cont'><br><br><h4>  Section  </h4><p> <b>  Insert Any Instructions For Section Below: </b> </p><textarea id = 'questionSec"+r+ "' class = 'questionSec' name = 'question"+r+ "' rows = '3'  ></textarea><br><img src = 'img/del_s.png' alt = 'click to delete!' id = 'delsec"+r+"' class = 'deleteSec'><br></div><hr>"
			data.setAttribute("id","sec"+j);
			data.setAttribute("class","section");
			secSet = true;
		}
		//check color for section creation
		if (secSet)
		{
			data.setAttribute("style", "background:" + colorArray[((r-1)%7)] + ";");
			$("#sideBar").css("background-color",colorArray[((r-1)%7)] + "!important");
			data.setAttribute("data-sectype", r);
		}
		//turn off anything that is in moving mode
		if ($('#allCustQ').hasClass('ui-sortable'))
		{
			if(	$("#button5").html() === "Edit Section");
			{
				$("#button5").html("Move Section");
				unMoveSec();
				var obj=document.getElementById("allCustQ");
				loopOver(obj);
				colorCheck();
				$("#sideBar").css("background-color",colorArray[((r-1)%7)]);
			}
			$("#button3").html("Move Questions");
			$( "#allCustQ" ).sortable("disable");
			$( "#allCustQ" ).enableSelection();
			$(".custQ").css("cursor","auto");
			$(".custQ").css("border-color","transparent");
			$(".section").css("cursor","auto");
			$(".section").css("border-color","transparent");
			var obj=document.getElementById("allCustQ");
			loopOver(obj);
			if($("#button4").html() === "Show All" && i > 0){
				pager.showPage(pager.currentPage);
			}
	}
	document.getElementById("allCustQ").appendChild(data);
	//icky page stuff//
		if($("#button4").html() === "Show All" && i > 0){
			var pageNavPosition = document.getElementById("pageNavPosition").id;
			pager.init(); 
			pager.showPageNav('pager', pageNavPosition);
			pager.showPage(pager.pages);
		}
	//array is used to get worth of question
	allQuest.push(0);
	//needed so a new section is shown in page view. 
	$("#sec"+ r).show();
}

//fix numbering cause I was an idiot and didn't use lists
function loopOver(obj)
    {
        var chl = document.getElementsByClassName("custQ");
		var spanTitle;
        for(var inc = 0;  inc < chl.length; inc++)
        {
			spanTitle = chl[inc].firstChild.children[0].innerHTML;
			chl[inc].firstChild.innerHTML = "#" + (inc + 1) + "<span class = 'qTitle'>"+spanTitle+"</span>";
        }
		
		var chl = document.getElementsByClassName("section")
		var spanTitle;
        for(var inc = 0;  inc < chl.length; inc++)
        {
			spanTitle = chl[inc].firstChild.children[0].innerHTML;
			chl[inc].firstChild.innerHTML = "Section #" + (inc + 1) + "<span class = 'qTitle'>"+spanTitle+"</span>";
        }
    }

//corrolate id number to position on dom
function getNumber(obj)
{

	var chl = document.getElementsByClassName("custQ");
    for(var inc = 0;  inc < chl.length; inc++)
	{	
		if(obj)
		{
			if (obj.id == chl[inc].id)
			{
				return inc + 1;
			}
		}
    }
	return 0;
}

//changes color based on section move
function colorCheck()
{
	var chl = document.getElementsByClassName("section");
	var trav = chl[0];
	var inc = 1;
	for(inc = 1; inc < s; inc++)
	{

		while(chl[inc] && trav.id != chl[inc].id && trav.hasAttribute("data-sectype"))
		{
			var type = parseInt(chl[inc-1].id.match(/(\d+)$/)[0], 10);
			trav.setAttribute("data-sectype", type);
			trav.style.background =  colorArray[((type-1)%7)];
			trav = trav.nextSibling;
		}

	}
	
	trav = chl[s-1];
	if(chl.length > 0 && chl[s-1])
	{
		r = parseInt(chl[s-1].id.match(/(\d+)$/)[0], 10);
	}
	while(trav && trav.hasAttribute("data-sectype"))
	{
		var type = parseInt(chl[s-1].id.match(/(\d+)$/)[0], 10);
		trav.setAttribute("data-sectype", type);
		trav.style.background =  colorArray[((type-1)%7)];
		trav = trav.nextSibling;
	}
}

//set a section up for drag by wraping it in a div
function moveSec()
{

	var chl = document.getElementsByClassName("section");
	var trav = document.getElementById("allCustQ").children[0];
	var inc = 1;
	var check = 1;
	for(inc = 1; inc < s; inc++)
	{
		var data=document.createElement("div");
		data.setAttribute("id","temp"+check);
		data.setAttribute("class","secMove");
		if(trav.hasAttribute("data-sectype"))
		{

			while(chl[inc] && trav.id != chl[inc].id && trav.hasAttribute("data-sectype"))
			{
				var type = parseInt(chl[inc-1].id.match(/(\d+)$/)[0], 10);
				trav.setAttribute("data-sectype", type);
				trav.style.background =  colorArray[((type-1)%7)];
				data.innerHTML += trav.outerHTML;
				trav = trav.nextSibling;
				
			
			}
		}
		else
		{
			data.innerHTML = trav.outerHTML;
			trav = trav.nextSibling;
			inc--;
		}
		check++;
		data.style.border = "1px solid transparent";
		data.style.borderRadius = "15px";
		document.getElementById("allCustQ").appendChild(data);
	}
	
	if(s < 2)
	{
		while(trav && trav.className != "section" && trav.className != "secMove" )
		{
			var data=document.createElement("div");
			data.setAttribute("id","temp"+check);
			data.setAttribute("class","secMove");
			data.style.border = "1px solid transparent";
			data.style.borderRadius = "15px";
			data.innerHTML = trav.outerHTML;
			document.getElementById("allCustQ").appendChild(data);
			trav = trav.nextSibling;
			check++;
		}
	}
	
	trav = chl[s-1];
	var data=document.createElement("div");
	data.setAttribute("id","temp"+(check));
	data.setAttribute("class","secMove");
	while(trav && trav.className != "secMove" && trav.hasAttribute("data-sectype"))
	{
		var type = parseInt(chl[s-1].id.match(/(\d+)$/)[0], 10);
		trav.setAttribute("data-sectype", type);
		trav.style.background =  colorArray[((type-1)%7)];
		data.innerHTML += trav.outerHTML;
		trav = trav.nextSibling;
	}
	data.style.border = "1px solid transparent";
	data.style.borderRadius = "15px";
	document.getElementById("allCustQ").appendChild(data);
	while(trav && trav.className != "secMove")
	{
		var data=document.createElement("div");
		data.setAttribute("id","temp"+check);
		data.setAttribute("class","secMove");
		data.innerHTML = trav.outerHTML;
		data.style.border = "1px solid transparent";
		data.style.borderRadius = "15px";
		document.getElementById("allCustQ").appendChild(data);
		trav = trav.nextSibling;
		check++;
	}
	
	
	var div = document.getElementById("allCustQ");
	var a;

	for (a=0;a<div.children.length;)
	{

		if (!(div.children[a].className == "secMove"))
		{
			$("#" + div.children[a].id).remove();
		}
		else{
			a++
		}
	}
	
	$( "#allCustQ" ).sortable( { containment: "#allCustQ" });
	$( "#allCustQ" ).sortable("enable");
	$( "#allCustQ" ).disableSelection();
}

//set the move back to normal
function unMoveSec(){
	var chl = document.getElementsByClassName("secMove");

	var inc;
	var end = document.getElementById("allCustQ");
	for(inc = 0; inc < chl.length; inc++)
	{

		end.innerHTML += chl[inc].innerHTML;
	
	}
	
	
	var div = document.getElementById("allCustQ");
	var a;

	for (a=0;a<div.children.length;)
	{

		if (div.children[a].className == "secMove")
		{
			$("#" + div.children[a].id).remove();
		}
		else{
			a++
		}
	}
	
	$( "#allCustQ" ).sortable("disable");
	$( "#allCustQ" ).enableSelection();

}

$(document).ready(function(){  
	
	$('#adOptions').hide();
	
	//slide the additional options
	$("#adOptionsBut").click(function(){
		$("#adOptions").slideToggle();
	});
	
  
	
	//all the following are hot keys for the templete code from earlier 
	$(document).jkey('n+d',function(){
		i = 0;
		j = 0;
		r = 0;
		s = 0;
		setSet = false;
		$("#allCustQ").children().remove();
		if ($('#allCustQ').hasClass('ui-sortable'))
		{
			$("#button3").html("Move Questions");
			$("#button5").html("Move Section");
			$( "#allCustQ" ).sortable("disable");
			$( "#allCustQ" ).enableSelection();
			$(".custQ").attr("style","cursor:auto; border-color:transparent;");
			var obj=document.getElementById("allCustQ");
			loopOver(obj);
			if ($("#button4").html() === "Show All")
			{
				pager.showPage(pager.currentPage);
			}
		}
		secSet = false;
		$("#sideBar").css("background-color","#F0FFFF");
		$("#pageNavPosition").html("<span class='pg-normal'> « Prev  </span> | <span id='pg1' class='pg-selected'>1</span> | <span class='pg-normal'>  Next »</span>");
		pager.currentPage = 1;
		pager.pages = 0;
	});
	
	$(document).jkey('n+1',function(){
		i += 1;
		j += 1;
		if($("#button").html() === "Maximize"){
			$("#button").html("Minimize");
			$('.small').show();
			$('#pool').show();
			$(".cont").slideToggle();
		}
		var imgName=$(this).attr('id');
		var data=document.createElement("div");
		data.setAttribute("id","q"+j);
		data.setAttribute("class","custQ");


		data.setAttribute("data-qtype","TF");
		data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'> <label for = 'p"+j+"'>Question Worth </label> <input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'> </div> <div class = 'cont'>  <br> <br> <h4 > True / False Question </h4> <p> <b>  Insert Question Below: </b> </p>  <input type = 'text'  class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'  > <br> <p> <b> Insert Answer Below: </b> </p> <input type = 'radio' name = 'ans"+j+"' id = 'ans"+j+"-1' value='True'/>True <br> <input type = 'radio' name = 'ans"+j+"' id = 'ans"+j+"-2' value='False'/> False <br> <img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'> <br> </div><hr>";
		numbAnsChoices.push(2);
		

	
		if (secSet)
		{
			data.setAttribute("style", "background:" + colorArray[((r-1)%7)] + ";");
			$("#sidebar").css("background",colorArray[((r-1)%7)]);
			data.setAttribute("data-sectype", r);
		}
		if ($('#allCustQ').hasClass('ui-sortable'))
		{
			if(	$("#button5").html() === "Edit Section");
			{
				$("#button5").html("Move Section");
				unMoveSec();
				var obj=document.getElementById("allCustQ");
				loopOver(obj);
				colorCheck();
				$("#sideBar").css("background-color",colorArray[((r-1)%7)]);
			}
			$("#button3").html("Move Questions");
			$( "#allCustQ" ).sortable("disable");
			$( "#allCustQ" ).enableSelection();
			$(".custQ").css("cursor","auto");
			$(".custQ").css("border-color","transparent");
			$(".section").css("cursor","auto");
			$(".section").css("border-color","transparent");
			var obj=document.getElementById("allCustQ");
			loopOver(obj);
			if($("#button4").html() === "Show All" && i > 0){
				pager.showPage(pager.currentPage);
			}
		}
		document.getElementById("allCustQ").appendChild(data);
		if($("#button4").html() === "Show All" && i > 0){
			var pageNavPosition = document.getElementById("pageNavPosition").id;
			pager.init(); 
			pager.showPageNav('pager', pageNavPosition);
			pager.showPage(pager.pages);
		}
		allQuest.push(0);
		$("#sec"+ r).show();
	});
	
	$(document).jkey('n+2',function(){
			i += 1;
		j += 1;
		if($("#button").html() === "Maximize"){
			$("#button").html("Minimize");
			$('.small').show();
			$('#pool').show();
			$(".cont").slideToggle();
		}
		var imgName=$(this).attr('id');
		var data=document.createElement("div");
		data.setAttribute("id","q"+j);
		data.setAttribute("class","custQ");



		data.setAttribute("data-qtype","MCO");
		data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label><input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'>	</div><div class = 'cont'> <br><br><h4 >  Multiple Choice (One Option) Question </h4><p> <b>  Insert Question Below: </b> </p> <input type = 'text'  class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'   ><br><p> <b> Insert Answer Below: </b> </p><input type='radio' name = 'ans"+j+"' id = 'ans"+j+"-1'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-1' ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'>	<br><input type='radio' name = 'ans"+j+"' id = 'ans"+j+"-2'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-2' ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'>	<br><input type='radio' name = 'ans"+j+"' id = 'ans"+j+"-3'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-3'  >	<img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><button type='button' class= 'addButton'>Add More Answers</button> <img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'><br>	</div> <hr>";
		numbAnsChoices.push(3);
		

		if (secSet)
		{
			data.setAttribute("style", "background:" + colorArray[((r-1)%7)] + ";");
			$("#sidebar").css("background",colorArray[((r-1)%7)]);
			data.setAttribute("data-sectype", r);
		}
		if ($('#allCustQ').hasClass('ui-sortable'))
		{
			if(	$("#button5").html() === "Edit Section");
			{
				$("#button5").html("Move Section");
				unMoveSec();
				var obj=document.getElementById("allCustQ");
				loopOver(obj);
				colorCheck();
				$("#sideBar").css("background-color",colorArray[((r-1)%7)]);
			}
			$("#button3").html("Move Questions");
			$( "#allCustQ" ).sortable("disable");
			$( "#allCustQ" ).enableSelection();
			$(".custQ").css("cursor","auto");
			$(".custQ").css("border-color","transparent");
			$(".section").css("cursor","auto");
			$(".section").css("border-color","transparent");
			var obj=document.getElementById("allCustQ");
			loopOver(obj);
			if($("#button4").html() === "Show All" && i > 0){
				pager.showPage(pager.currentPage);
			}
		}
		document.getElementById("allCustQ").appendChild(data);
		if($("#button4").html() === "Show All" && i > 0){
			var pageNavPosition = document.getElementById("pageNavPosition").id;
			pager.init(); 
			pager.showPageNav('pager', pageNavPosition);
			pager.showPage(pager.pages);
		}
		allQuest.push(0);
		$("#sec"+ r).show();
	});
	
	$(document).jkey('n+3',function(){
		i += 1;
		j += 1;
		if($("#button").html() === "Maximize"){
			$("#button").html("Minimize");
			$('.small').show();
			$('#pool').show();
			$(".cont").slideToggle();
		}
		var imgName=$(this).attr('id');
		var data=document.createElement("div");
		data.setAttribute("id","q"+j);
		data.setAttribute("class","custQ");



		data.setAttribute("data-qtype","MCA");
		data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label>	<input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'>	</div><div class = 'cont'> <br><br><h4>  Multiple Choice (All Options) Question </h4> <p> <b>  Insert Question Below: </b> </p><input type = 'text' class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'  ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><p> <b> Insert Answer Below: </b> </p>	<input type='checkbox' name = 'ans"+j+"-1' id = 'ans"+j+"-1'/><input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-1'  ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><input type='checkbox' name = 'ans"+j+"' id = 'ans"+j+"-2'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-2'  ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><input type='checkbox' name = 'ans"+j+"' id = 'ans"+j+"-3'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-3'  ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><button type='button' class= 'addButton'>Add More Answers</button>	<img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'><br></div><hr>";
		numbAnsChoices.push(3);
		


		if (secSet)
		{
			data.setAttribute("style", "background:" + colorArray[((r-1)%7)] + ";");
			$("#sidebar").css("background",colorArray[((r-1)%7)]);
			data.setAttribute("data-sectype", r);
		}
		if ($('#allCustQ').hasClass('ui-sortable'))
		{
			if(	$("#button5").html() === "Edit Section");
			{
				$("#button5").html("Move Section");
				unMoveSec();
				var obj=document.getElementById("allCustQ");
				loopOver(obj);
				colorCheck();
				$("#sideBar").css("background-color",colorArray[((r-1)%7)]);
			}
			$("#button3").html("Move Questions");
			$( "#allCustQ" ).sortable("disable");
			$( "#allCustQ" ).enableSelection();
			$(".custQ").css("cursor","auto");
			$(".custQ").css("border-color","transparent");
			$(".section").css("cursor","auto");
			$(".section").css("border-color","transparent");
			var obj=document.getElementById("allCustQ");
			loopOver(obj);
			if($("#button4").html() === "Show All" && i > 0){
				pager.showPage(pager.currentPage);
			}
		}
		document.getElementById("allCustQ").appendChild(data);
		if($("#button4").html() === "Show All" && i > 0){
			var pageNavPosition = document.getElementById("pageNavPosition").id;
			pager.init(); 
			pager.showPageNav('pager', pageNavPosition);
			pager.showPage(pager.pages);
		}
		allQuest.push(0);
		$("#sec"+ r).show();
	});
	
	$(document).jkey('n+4',function(){
		i += 1;
		j += 1;
		if($("#button").html() === "Maximize"){
			$("#button").html("Minimize");
			$('.small').show();
			$('#pool').show();
			$(".cont").slideToggle();
		}
		var imgName=$(this).attr('id');
		var data=document.createElement("div");
		data.setAttribute("id","q"+j);
		data.setAttribute("class","custQ");


		data.setAttribute("data-qtype","FITWE");
		data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label><input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'>	</div>	<div class = 'cont'><br>	<br><h4>  Fill In the Blank - End of Sentence Question </h4> <p> <b>  Insert  Question Below: </b> </p>  <input type = 'text' class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'  ><br><p> <b> Insert Answer Below: </b> </p><input type='text' id='ans"+j+"-1' class='Questions_FITB' autocomplete='off' /> <br><img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'><br></div><hr>";
		numbAnsChoices.push(1);
		


		if (secSet)
		{
			data.setAttribute("style", "background:" + colorArray[((r-1)%7)] + ";");
			$("#sidebar").css("background",colorArray[((r-1)%7)]);
			data.setAttribute("data-sectype", r);
		}
		if ($('#allCustQ').hasClass('ui-sortable'))
		{
			if(	$("#button5").html() === "Edit Section");
			{
				$("#button5").html("Move Section");
				unMoveSec();
				var obj=document.getElementById("allCustQ");
				loopOver(obj);
				colorCheck();
				$("#sideBar").css("background-color",colorArray[((r-1)%7)]);
			}
			$("#button3").html("Move Questions");
			$( "#allCustQ" ).sortable("disable");
			$( "#allCustQ" ).enableSelection();
			$(".custQ").css("cursor","auto");
			$(".custQ").css("border-color","transparent");
			$(".section").css("cursor","auto");
			$(".section").css("border-color","transparent");
			var obj=document.getElementById("allCustQ");
			loopOver(obj);
			if($("#button4").html() === "Show All" && i > 0){
				pager.showPage(pager.currentPage);
			}
		}
		document.getElementById("allCustQ").appendChild(data);
		if($("#button4").html() === "Show All" && i > 0){
			var pageNavPosition = document.getElementById("pageNavPosition").id;
			pager.init(); 
			pager.showPageNav('pager', pageNavPosition);
			pager.showPage(pager.pages);
		}
		allQuest.push(0);
		$("#sec"+ r).show();
	});
	
	$(document).jkey('n+5',function(){
		i += 1;
		j += 1;
		if($("#button").html() === "Maximize"){
			$("#button").html("Minimize");
			$('.small').show();
			$('#pool').show();
			$(".cont").slideToggle();
		}
		var imgName=$(this).attr('id');
		var data=document.createElement("div");
		data.setAttribute("id","q"+j);
		data.setAttribute("class","custQ");


	
		data.setAttribute("data-qtype","FITWM");
		data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label>	<input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'></div><div class = 'cont'><br><br><h4>  Fill In the Blank - Middle of Sentence Question </h4> <p> <b>  Insert Beginning of Question Below: </b> </p> <input type = 'text'  class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'><br><p> <b> Insert Answer Below: </b> </p><input type='text' id='ans"+j+"-1' class='Questions_FITB' autocomplete='off' /> <br> <p> <b>  Insert End of Question Below: </b> </p> <input type = 'text'  class = 'questionBlanked' id='ans"+j+"-2' > <br><img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'> <br></div><hr> ";
		numbAnsChoices.push(2);

		if (secSet)
		{
			data.setAttribute("style", "background:" + colorArray[((r-1)%7)] + ";");
			$("#sidebar").css("background",colorArray[((r-1)%7)]);
			data.setAttribute("data-sectype", r);
		}
		if ($('#allCustQ').hasClass('ui-sortable'))
		{
			if(	$("#button5").html() === "Edit Section");
			{
				$("#button5").html("Move Section");
				unMoveSec();
				var obj=document.getElementById("allCustQ");
				loopOver(obj);
				colorCheck();
				$("#sideBar").css("background-color",colorArray[((r-1)%7)]);
			}
			$("#button3").html("Move Questions");
			$( "#allCustQ" ).sortable("disable");
			$( "#allCustQ" ).enableSelection();
			$(".custQ").css("cursor","auto");
			$(".custQ").css("border-color","transparent");
			$(".section").css("cursor","auto");
			$(".section").css("border-color","transparent");
			var obj=document.getElementById("allCustQ");
			loopOver(obj);
			if($("#button4").html() === "Show All" && i > 0){
				pager.showPage(pager.currentPage);
			}
		}
		document.getElementById("allCustQ").appendChild(data);
		if($("#button4").html() === "Show All" && i > 0){
			var pageNavPosition = document.getElementById("pageNavPosition").id;
			pager.init(); 
			pager.showPageNav('pager', pageNavPosition);
			pager.showPage(pager.pages);
		}
		allQuest.push(0);
		$("#sec"+ r).show();
	});
	
	$(document).jkey('n+6',function(){
		i += 1;
		j += 1;
		if($("#button").html() === "Maximize"){
			$("#button").html("Minimize");
			$('.small').show();
			$('#pool').show();
			$(".cont").slideToggle();
		}
		var imgName=$(this).attr('id');
		var data=document.createElement("div");
		data.setAttribute("id","q"+j);
		data.setAttribute("class","custQ");

		data.setAttribute("data-qtype","SA");
		data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label><input type = 'text' name = 'p"+j+"' id = 'p"+j+"' class = 'worth'></div><div class = 'cont'><br><br><h4>  Short Answer Question </h4><p> <b>  Insert Question Below: </b> </p><textarea id = 'question"+j+ "' class = 'questionBlanked' name = 'question"+j+ "' rows = '3'  ></textarea><br><img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'><br></div><hr>";
		numbAnsChoices.push(0);
		

		if (secSet)
		{
			data.setAttribute("style", "background:" + colorArray[((r-1)%7)] + ";");
			$("#sidebar").css("background",colorArray[((r-1)%7)]);
			data.setAttribute("data-sectype", r);
		}
		if ($('#allCustQ').hasClass('ui-sortable'))
		{
			if(	$("#button5").html() === "Edit Section");
			{
				$("#button5").html("Move Section");
				unMoveSec();
				var obj=document.getElementById("allCustQ");
				loopOver(obj);
				colorCheck();
				$("#sideBar").css("background-color",colorArray[((r-1)%7)]);
			}
			$("#button3").html("Move Questions");
			$( "#allCustQ" ).sortable("disable");
			$( "#allCustQ" ).enableSelection();
			$(".custQ").css("cursor","auto");
			$(".custQ").css("border-color","transparent");
			$(".section").css("cursor","auto");
			$(".section").css("border-color","transparent");
			var obj=document.getElementById("allCustQ");
			loopOver(obj);
			if($("#button4").html() === "Show All" && i > 0){
				pager.showPage(pager.currentPage);
			}
		}
		document.getElementById("allCustQ").appendChild(data);
		if($("#button4").html() === "Show All" && i > 0){
			var pageNavPosition = document.getElementById("pageNavPosition").id;
			pager.init(); 
			pager.showPageNav('pager', pageNavPosition);
			pager.showPage(pager.pages);
		}
		allQuest.push(0);
		$("#sec"+ r).show();
	});
	
	//if you use enter when you tab to a question
	$('.Questions').on('keyup', '.create', function(e){
		if(e.which === 13) {
		i += 1;
		j += 1;
		if($("#button").html() === "Maximize"){
			$("#button").html("Minimize");
			$('.small').show();
			$('#pool').show();
			$(".cont").slideToggle();
		}
		var imgName=$(this).attr('id');
		var data=document.createElement("div");
		data.setAttribute("id","q"+j);
		data.setAttribute("class","custQ");

		if(imgName==="TFCreate"){
			data.setAttribute("data-qtype","TF");
			data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'> <label for = 'p"+j+"'>Question Worth </label> <input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'> </div> <div class = 'cont'>  <br> <br> <h4 > True / False Question </h4> <p> <b>  Insert Question Below: </b> </p>  <input type = 'text'  class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'  > <br> <p> <b> Insert Answer Below: </b> </p> <input type = 'radio' name = 'ans"+j+"' id = 'ans"+j+"-1' value='True'/>True <br> <input type = 'radio' name = 'ans"+j+"' id = 'ans"+j+"-2' value='False'/> False <br> <img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'> <br> </div><hr>";
			numbAnsChoices.push(2);
		}

		if( imgName==="MCOCreate"){
			data.setAttribute("data-qtype","MCO");
			data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label><input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'>	</div><div class = 'cont'> <br><br><h4 >  Multiple Choice (One Option) Question </h4><p> <b>  Insert Question Below: </b> </p> <input type = 'text'  class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'   ><br><p> <b> Insert Answer Below: </b> </p><input type='radio' name = 'ans"+j+"' id = 'ans"+j+"-1'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-1' ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'>	<br><input type='radio' name = 'ans"+j+"' id = 'ans"+j+"-2'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-2' ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'>	<br><input type='radio' name = 'ans"+j+"' id = 'ans"+j+"-3'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-3'  >	<img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><button type='button' class= 'addButton'>Add More Answers</button> <img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'><br>	</div> <hr>";
			numbAnsChoices.push(3);
		}

		if(imgName==="MCACreate"){
			data.setAttribute("data-qtype","MCA");
			data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label>	<input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'>	</div><div class = 'cont'> <br><br><h4>  Multiple Choice (All Options) Question </h4> <p> <b>  Insert Question Below: </b> </p><input type = 'text' class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'  ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><p> <b> Insert Answer Below: </b> </p>	<input type='checkbox' name = 'ans"+j+"-1' id = 'ans"+j+"-1'/><input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-1'  ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><input type='checkbox' name = 'ans"+j+"' id = 'ans"+j+"-2'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-2'  ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><input type='checkbox' name = 'ans"+j+"' id = 'ans"+j+"-3'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-3'  ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><button type='button' class= 'addButton'>Add More Answers</button>	<img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'><br></div><hr>";
			numbAnsChoices.push(3);
		}
		if(imgName==="FITWECreate"){
			data.setAttribute("data-qtype","FITWE");
			data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label><input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'>	</div>	<div class = 'cont'><br>	<br><h4>  Fill In the Blank - End of Sentence Question </h4> <p> <b>  Insert  Question Below: </b> </p>  <input type = 'text' class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'  ><br><p> <b> Insert Answer Below: </b> </p><input type='text' id='ans"+j+"-1' class='Questions_FITB' autocomplete='off' /> <br><img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'><br></div><hr>";
			numbAnsChoices.push(1);
		}
		if(imgName==="FITWMCreate"){
			data.setAttribute("data-qtype","FITWM");
			data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label>	<input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'></div><div class = 'cont'><br><br><h4>  Fill In the Blank - Middle of Sentence Question </h4> <p> <b>  Insert Beginning of Question Below: </b> </p> <input type = 'text'  class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'><br><p> <b> Insert Answer Below: </b> </p><input type='text' id='ans"+j+"-1' class='Questions_FITB' autocomplete='off' /> <br> <p> <b>  Insert End of Question Below: </b> </p> <input type = 'text'  class = 'questionBlanked' id='ans"+j+"-2' > <br><img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'> <br></div><hr> ";
			numbAnsChoices.push(2);
		}
		if(imgName==="SACreate"){
			data.setAttribute("data-qtype","SA");
			data.innerHTML = "<h3> #"+s+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label><input type = 'text' name = 'p"+j+"' id = 'p"+j+"' class = 'worth'></div><div class = 'cont'><br><br><h4>  Short Answer Question </h4><p> <b>  Insert Question Below: </b> </p><textarea id = 'question"+j+ "' class = 'questionBlanked' name = 'question"+j+ "' rows = '3'  ></textarea><br><img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'><br></div><hr>";
			numbAnsChoices.push(0);
		}
		if(imgName==="SeCreate"){
			data.setAttribute("data-qtype","Se");
			r++;
			s++;
			i--;
			j--;
			data.innerHTML = "<h3> Section #" +r + "<span class = 'qTitle'>	</span></h3><div class = 'cont'><br><br><h4>  Section  </h4><p> <b>  Insert Any Instructions For Section Below: </b> </p><textarea id = 'questionSec"+r+ "' class = 'questionSec' name = 'question"+r+ "' rows = '3'  ></textarea><br><img src = 'img/del_s.png' alt = 'click to delete!' id = 'delsec"+r+"' class = 'deleteSec'><br></div><hr>"
			data.setAttribute("id","sec"+j);
			data.setAttribute("class","section");
			secSet = true;
		}

		if (secSet)
		{
			data.setAttribute("style", "background:" + colorArray[((r-1)%7)] + ";");
			$("#sidebar").css("background",colorArray[((r-1)%7)]);
			data.setAttribute("data-sectype", r);
		}
		if ($('#allCustQ').hasClass('ui-sortable'))
		{
			if(	$("#button5").html() === "Edit Section");
			{
				$("#button5").html("Move Section");
				unMoveSec();
				var obj=document.getElementById("allCustQ");
				loopOver(obj);
				colorCheck();
				$("#sideBar").css("background-color",colorArray[((r-1)%7)]);
			}
			$("#button3").html("Move Questions");
			$( "#allCustQ" ).sortable("disable");
			$( "#allCustQ" ).enableSelection();
			$(".custQ").css("cursor","auto");
			$(".custQ").css("border-color","transparent");
			$(".section").css("cursor","auto");
			$(".section").css("border-color","transparent");
			var obj=document.getElementById("allCustQ");
			loopOver(obj);
			if($("#button4").html() === "Show All" && i > 0){
				pager.showPage(pager.currentPage);
			}
		}
		document.getElementById("allCustQ").appendChild(data);
		if($("#button4").html() === "Show All" && i > 0){
			var pageNavPosition = document.getElementById("pageNavPosition").id;
			pager.init(); 
			pager.showPageNav('pager', pageNavPosition);
			pager.showPage(pager.pages);
		}
		allQuest.push(0);
		$("#sec"+ r).show();
		}
    });
	
	//if you want to click to create a question
	$(".create").click(function(){
		i += 1;
		j += 1;
		if($("#button").html() === "Maximize"){
			$("#button").html("Minimize");
			$('.small').show();
			$('#pool').show();
			$(".cont").slideToggle();
		}
		var imgName=$(this).attr('id');
		var data=document.createElement("div");
		data.setAttribute("id","q"+j);
		data.setAttribute("class","custQ");

		if(imgName==="TFCreate"){
			data.setAttribute("data-qtype","TF");
			data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'> <label for = 'p"+j+"'>Question Worth </label> <input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'> </div> <div class = 'cont'>  <br> <br> <h4 > True / False Question </h4> <p> <b>  Insert Question Below: </b> </p>  <input type = 'text'  class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'  > <br> <p> <b> Insert Answer Below: </b> </p> <input type = 'radio' name = 'ans"+j+"' id = 'ans"+j+"-1' value='True'/>True <br> <input type = 'radio' name = 'ans"+j+"' id = 'ans"+j+"-2' value='False'/> False <br> <img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'> <br> </div><hr>";
			numbAnsChoices.push(2);
		}

		if( imgName==="MCOCreate"){
			data.setAttribute("data-qtype","MCO");
			data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label><input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'>	</div><div class = 'cont'> <br><br><h4 >  Multiple Choice (One Option) Question </h4><p> <b>  Insert Question Below: </b> </p> <input type = 'text'  class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'   ><br><p> <b> Insert Answer Below: </b> </p><input type='radio' name = 'ans"+j+"' id = 'ans"+j+"-1'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-1' ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'>	<br><input type='radio' name = 'ans"+j+"' id = 'ans"+j+"-2'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-2' ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'>	<br><input type='radio' name = 'ans"+j+"' id = 'ans"+j+"-3'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-3'  >	<img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><button type='button' class= 'addButton'>Add More Answers</button> <img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'><br>	</div> <hr>";
			numbAnsChoices.push(3);
		}

		if(imgName==="MCACreate"){
			data.setAttribute("data-qtype","MCA");
			data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label>	<input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'>	</div><div class = 'cont'> <br><br><h4>  Multiple Choice (All Options) Question </h4> <p> <b>  Insert Question Below: </b> </p><input type = 'text' class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'  ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><p> <b> Insert Answer Below: </b> </p>	<input type='checkbox' name = 'ans"+j+"-1' id = 'ans"+j+"-1'/><input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-1'  ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><input type='checkbox' name = 'ans"+j+"' id = 'ans"+j+"-2'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-2'  ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><input type='checkbox' name = 'ans"+j+"' id = 'ans"+j+"-3'/>	<input type = 'text' name = 'ansN"+j+"' id = 'ansN"+j+"-3'  ><img src = 'img/x.png' alt = 'delete answer!' id = 'delA"+j+"-1' class = 'deleteAns'><br><button type='button' class= 'addButton'>Add More Answers</button>	<img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'><br></div><hr>";
			numbAnsChoices.push(3);
		}
		if(imgName==="FITWECreate"){
			data.setAttribute("data-qtype","FITWE");
			data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label><input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'>	</div>	<div class = 'cont'><br>	<br><h4>  Fill In the Blank - End of Sentence Question </h4> <p> <b>  Insert  Question Below: </b> </p>  <input type = 'text' class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'  ><br><p> <b> Insert Answer Below: </b> </p><input type='text' id='ans"+j+"-1' class='Questions_FITB' autocomplete='off' /> <br><img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'><br></div><hr>";
			numbAnsChoices.push(1);
		}
		if(imgName==="FITWMCreate"){
			data.setAttribute("data-qtype","FITWM");
			data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label>	<input type = 'text' name = 'p"+j+"' id = 'p"+j+"'  class = 'worth'></div><div class = 'cont'><br><br><h4>  Fill In the Blank - Middle of Sentence Question </h4> <p> <b>  Insert Beginning of Question Below: </b> </p> <input type = 'text'  class = 'questionBlanked' id = 'question"+j+ "' name = 'question"+j+ "'><br><p> <b> Insert Answer Below: </b> </p><input type='text' id='ans"+j+"-1' class='Questions_FITB' autocomplete='off' /> <br> <p> <b>  Insert End of Question Below: </b> </p> <input type = 'text'  class = 'questionBlanked' id='ans"+j+"-2' > <br><img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'> <br></div><hr> ";
			numbAnsChoices.push(2);
		}
		if(imgName==="SACreate"){
			data.setAttribute("data-qtype","SA");
			data.innerHTML = "<h3> #"+i+" <span class = 'qTitle'></span></h3><div class = 'small'><label for = 'p"+j+"'>Question Worth </label><input type = 'text' name = 'p"+j+"' id = 'p"+j+"' class = 'worth'></div><div class = 'cont'><br><br><h4>  Short Answer Question </h4><p> <b>  Insert Question Below: </b> </p><textarea id = 'question"+j+ "' class = 'questionBlanked' name = 'question"+j+ "' rows = '3'  ></textarea><br><img src = 'img/del_Q.png' alt = 'click to delete!' id = 'del"+j+"' class = 'delete'><br></div><hr>";
			numbAnsChoices.push(0);
		}
		if(imgName==="SeCreate"){
			data.setAttribute("data-qtype","Se");
			r++;
			s++;
			i--;
			j--;
			data.innerHTML = "<h3> Section #" +s + "<span class = 'qTitle'>	</span></h3><div class = 'cont'><br><br><h4>  Section  </h4><p> <b>  Insert Any Instructions For Section Below: </b> </p><textarea id = 'questionSec"+r+ "' class = 'questionSec' name = 'question"+r+ "' rows = '3'  ></textarea><br><img src = 'img/del_s.png' alt = 'click to delete!' id = 'delsec"+r+"' class = 'deleteSec'><br></div><hr>"
			data.setAttribute("id","sec"+r);
			data.setAttribute("class","section");
			secSet = true;
		}
		
		if (secSet)
		{
			data.setAttribute("style", "background:" + colorArray[((r-1)%7)] + ";");
			$("#sideBar").css("background-color",colorArray[((r-1)%7)] + "!important");
			data.setAttribute("data-sectype", r);

		}

		if ($('#allCustQ').hasClass('ui-sortable'))
		{
			if(	$("#button5").html() === "Edit Section");
			{
				$("#button5").html("Move Section");
				unMoveSec();
				var obj=document.getElementById("allCustQ");
				loopOver(obj);
				colorCheck();
				$("#sideBar").css("background-color",colorArray[((r-1)%7)]);
			}
			$("#button3").html("Move Questions");
			$( "#allCustQ" ).sortable("disable");
			$( "#allCustQ" ).enableSelection();
			$(".custQ").css("cursor","auto");
			$(".custQ").css("border-color","transparent");
			$(".section").css("cursor","auto");
			$(".section").css("border-color","transparent");
			var obj=document.getElementById("allCustQ");
			loopOver(obj);
			if($("#button4").html() === "Show All" && i > 0){
				pager.showPage(pager.currentPage);
			}
		}
		document.getElementById("allCustQ").appendChild(data);
		if($("#button4").html() === "Show All" && i > 0){
			var pageNavPosition = document.getElementById("pageNavPosition").id;
			pager.init(); 
			pager.showPageNav('pager', pageNavPosition);
			pager.showPage(pager.pages);
		}
		$("#sec"+ r).show();
		allQuest.push(0);

	});
	

	//shows how many questions you want per page
	$('#title').on('change', '#numQPerPage', function(e){
		pager.itemsPerPage = parseInt($(this).val(),10);
		if (qSet == true)
		{
			$("#allCustQ").children().show();
			var pageNavPosition = document.getElementById("pageNavPosition").id;
			pager.init(); 
			pager.showPageNav('pager', pageNavPosition);
			pager.showPage(pager.pages);
		}
		qSet = true;
    });
	
/* add up points */
	$('#quiz').on('keyup', '.worth', function(e){
		var num = parseInt($(this).val(),10);
		var k = parseInt($(this).attr('id').match(/(\d+)$/)[0], 10);
		if (isNaN(num) || num < 0)
		{
			num = 100;
		}
		allQuest[k-1] = num;
		var result = 0;

		for (var j = 0; j < allQuest.length; j++) {
			result += allQuest[j];
		}
		$("#pointTally").html("Number of Points Used: " + result + "");
		QM134b_Test.testWorth = result;
		var totPoints = parseInt($("#pointsQuiz").val(),10);

		if (isNaN(totPoints) || totPoints < 0)
		{
			totPoints = 0;
		}
		if(totPoints !== 0 && totPoints < result)
		{
			$("#pointTally").attr("style","color:red");
		}
		else
		{
			$("#pointTally").attr("style","color:black");
		}
		num = null;
		k = null;
		result = null;
    });
	
	//sets the title of the question dynamic to what is getting asked
	$('#quiz').on('keyup', '.questionBlanked', function(e){
		var title = $(this).val();
		strTitle = title.substring(0,30);
		var k = parseInt($(this).attr('id').match(/(\d+)$/)[0], 10);
		if (title.length > 30)
		{
			document.getElementById("q"+k).firstChild.children[0].innerHTML = "- " + strTitle + "...";
		}
		else
		{
			document.getElementById("q"+k).firstChild.children[0].innerHTML = "- " + strTitle;
		}
    });
	
	/* delete individual question */
	$('#quiz').on('click', '.delete', function(e){
		var k = parseInt($(this).attr('id').match(/(\d+)$/)[0], 10);
		i -= 1;
		allQuest[k-1] = 0;
		var result = 0;
		for (var j = 0; j < allQuest.length; j++) {
			result += allQuest[j];
		}
		$("#pointTally").html("Number of Points Used: " + result + "");
		var totPoints = parseInt($("#pointsQuiz").val(),10);

		if (isNaN(totPoints) || totPoints < 0)
		{
			totPoints = 0;
		}
		if(totPoints !== 0 && totPoints < result)
		{
			$("#pointTally").attr("style","color:red");
		}
		else
		{
			$("#pointTally").attr("style","color:black");
		}
		var l = getNumber(document.getElementById("q" + k));
		$("#q" + k).empty();
		$("#q" + k).remove();
		if ((pager.currentPage - 1) * pager.itemsPerPage + 1 == l && pager.currentPage != 1  )
		{
			pager.currentPage -= 1;
		}
		if($(".custQ").length != 0)
		{
			var pageNavPosition = document.getElementById("pageNavPosition").id;
			pager.init(); 
			pager.showPageNav('pager', pageNavPosition);
			pager.showPage(pager.pages);
		}
		var obj=document.getElementById("allCustQ");
		loopOver(obj);
		k = null;
	});
	
	//delete whole section
	$('#quiz').on('click', '.deleteSec', function(e){
		var p = parseInt($(this).attr('id').match(/(\d+)$/)[0], 10);
		s--;
		for (var inc = i-1; inc >= 0; inc--){
		//I dont know how to call jquery functions ahhhhh
			if($(".custQ").eq(inc).attr("data-sectype") == p){

				var k = parseInt($(".custQ").eq(inc).attr("id").match(/(\d+)$/)[0], 10);
				i -= 1;
				allQuest[k-1] = 0;
				var result = 0;
				for (var j = 0; j < allQuest.length; j++) {
					result += allQuest[j];
				}
				$("#pointTally").html("Number of Points Used: " + result + "");
				var totPoints = parseInt($("#pointsQuiz").val(),10);

				if (isNaN(totPoints) || totPoints < 0)
				{
					totPoints = 0;
				}
				if(totPoints !== 0 && totPoints < result)
				{
					$("#pointTally").attr("style","color:red");
				}
				else
				{
					$("#pointTally").attr("style","color:black");
				}
				var l = getNumber(document.getElementById("q" + k));
				$("#q" + k).empty();
				$("#q" + k).remove();
				if ((pager.currentPage - 1) * pager.itemsPerPage + 1 == l && pager.currentPage != 1  )
				{
					pager.currentPage -= 1;
				}
				if($(".custQ").length != 0)
				{
					var pageNavPosition = document.getElementById("pageNavPosition").id;
					pager.init(); 
					pager.showPageNav('pager', pageNavPosition);
					pager.showPage(pager.pages);
				}
				var obj=document.getElementById("allCustQ");
				loopOver(obj);
				k = null;
				}
		}
		
			if ($("#button4").html() === "Show All" )
			{
			if($(".custQ").length != 0)
				{
				$("#pageNavPosition").show();
				var pageNavPosition = document.getElementById("pageNavPosition").id;
				pager.init(); 
				pager.showPageNav('pager', pageNavPosition);
				pager.showPage(pager.pages);
				pager.showPage(pager.currentPage);
				}
			}
		$("#sec" + p).empty();
		$("#sec" + p).remove();
		var obj=document.getElementById("allCustQ");
		loopOver(obj);
		colorCheck();
		var secSet = false; 
		$("#sideBar").css("background-color","#F0FFFF");
	});


	//clear the section
	$("#clearSes").click(function(){
		secSet = false;
		$("#sideBar").css("background-color","#F0FFFF");
	});
	
	//minimize the bar
	$("#button").click(function(){
		if($(this).html() === "Minimize"){
			$(this).html("Maximize");
			$('.small').hide();
			$('#pool').hide();
		}
		else{
			$(this).html("Minimize");
			$('.small').show();
			$('#pool').show();
		}
		$(".cont").slideToggle();
	});
	
	/* create button using click*/
	$("#button2").click(function(){
		i = 0;
		j = 0;
		r = 0;
		s = 0;
		setSet = false;
		$("#allCustQ").children().remove();
		if ($('#allCustQ').hasClass('ui-sortable'))
		{
			$("#button3").html("Move Questions");
			$("#button5").html("Move Section");
			$( "#allCustQ" ).sortable("disable");
			$( "#allCustQ" ).enableSelection();
			$(".custQ").attr("style","cursor:auto; border-color:transparent;");
			var obj=document.getElementById("allCustQ");
			loopOver(obj);
			if ($("#button4").html() === "Show All")
			{
				pager.showPage(pager.currentPage);
			}
		}
		secSet = false;
		$("#sideBar").css("background-color","#F0FFFF");
		$("#pageNavPosition").html("<span class='pg-normal'> « Prev  </span> | <span id='pg1' class='pg-selected'>1</span> | <span class='pg-normal'>  Next »</span>");
		pager.currentPage = 1;
		pager.pages = 0;

	});
	
	/* for drag n drop*/
	$("#button3").click(function() {
		if($(this).html() === "Move Questions"){
		if($("#button5").html() === "Move Section"){
			if(document.getElementById("allCustQ").children[0])
			{
				$("#allCustQ").children().show();
				$("#pageNavPosition").hide();
				$(this).html("Edit Questions");
				$( "#allCustQ" ).sortable( { containment: "#allCustQ" });
				$( "#allCustQ" ).sortable("enable");
				$( "#allCustQ" ).disableSelection();
				$(".custQ").css("cursor","pointer");
				$(".custQ").css("border-color","black");
				$(".section").css("cursor","pointer");
				$(".section").css("border-color","black");
				$(".section").css("opacity","1");

			}
			}
		}
		else{
			$(this).html("Move Questions");
			$( "#allCustQ" ).sortable("disable");
			$( "#allCustQ" ).enableSelection();
			$(".custQ").css("cursor","auto");
			$(".custQ").css("border-color","transparent");
			$(".section").css("cursor","auto");
			$(".section").css("border-color","transparent");
			var obj=document.getElementById("allCustQ");
			loopOver(obj);
			colorCheck();
			$("#sideBar").css("background-color",colorArray[((r-1)%7)]);
			if ($("#button4").html() === "Show All" )
			{
			if($(".custQ").length != 0)
				{
				$("#pageNavPosition").show();
				var pageNavPosition = document.getElementById("pageNavPosition").id;
				pager.init(); 
				pager.showPageNav('pager', pageNavPosition);
				pager.showPage(pager.pages);
				pager.showPage(pager.currentPage);
				}
			}
		}

	});
	
	//page or show all mode
	$("#button4").click(function() {
		if($(this).html() === "Show All"){
			$("#allCustQ").children().show();
			$("#pageNavPosition").hide();
			$(this).html("Show Page");
		}
		else{
			$(this).html("Show All");
			if($(".custQ").length != 0)
				{
				$("#pageNavPosition").show();
				var pageNavPosition = document.getElementById("pageNavPosition").id;
				pager.init(); 
				pager.showPageNav('pager', pageNavPosition);
				pager.showPage(pager.pages);
				pager.showPage(pager.currentPage);
				}
		}

	});
	
	//for moving a section it checks to see what mode it's in and calls the moving function based on that
	$("#button5").click(function() {
		if($(this).html() === "Move Section"){
		if($("#button3").html() === "Move Questions")
		{
			$(this).html("Edit Section");
			moveSec();
			$(".secMove").css("cursor","pointer");
			$(".secMove").css("border-color","black");
		}
		}
		else{
		$(this).html("Move Section");
		unMoveSec();
		var obj=document.getElementById("allCustQ");
		loopOver(obj);
		colorCheck();
		$("#sideBar").css("background-color",colorArray[((r-1)%7)]);
		if ($("#button4").html() === "Show All" )
		{
		if($(".custQ").length != 0)
			{
			$("#pageNavPosition").show();
			var pageNavPosition = document.getElementById("pageNavPosition").id;
			pager.init(); 
			pager.showPageNav('pager', pageNavPosition);
			pager.showPage(pager.pages);
			pager.showPage(pager.currentPage);
			}
		}
		}

	});
	
	
//add individal answer
$('#quiz').on('click', '.addButton', function(e){
	var k = $(this).parent().parent();
	var divNum = parseInt($(k).attr('id').match(/(\d+)$/)[0], 10);
	
	var type = $(k).attr('data-qtype');
	var prevSib = $(this).prev().prev();
	var incrNum = 0;
	if(($(prevSib).is('[id]')))
	{
		incrNum =  parseInt($(prevSib).attr('id').match(/(\d+)$/)[0], 10);
	}
	if(incrNum === 1)
	{
		incrNum = 3;
	}
	incrNum += 1; 
	var inputA = document.createElement("input");
	if(type === 'MCO')
	{
		inputA.setAttribute("type","radio");
	}
	else
	{
		inputA.setAttribute("type","checkbox");
	}

	numbAnsChoices[divNum-1]+=1;

	inputA.setAttribute("name","ans"+divNum);
	inputA.setAttribute("id","ans"+divNum+"-"+incrNum);
	
	$(inputA).insertBefore($("#" + $(k).attr('id') + " .addButton"));
	
	var inputB = document.createElement("input");
	inputB.setAttribute("type","text");
	inputB.setAttribute("name","ansN"+divNum);
	inputB.setAttribute("id","ansN"+divNum+"-"+incrNum);
	inputB.setAttribute("style","margin-left:14px;");
	$(inputB).insertBefore($("#" + $(k).attr('id') + " .addButton"));
	var inputC	= document.createElement("img");
	inputC.setAttribute("src","img/x.png");
	inputC.setAttribute("alt","delete answer!");
	inputC.setAttribute("class","deleteAns");
	inputC.setAttribute("id","delA"+divNum+"-"+incrNum);
	$(inputC).insertBefore($("#" + $(k).attr('id') + " .addButton"));
	$(document.createElement("br")).insertBefore($("#" + $(k).attr('id') + " .addButton"));
	});

//removes an answer
$('#quiz').on('click', '.deleteAns', function(e){
		$($(this).prev().prev()).remove();
		$($(this).prev()).remove();
		$($(this).next()).remove();
		$($(this)).remove();
	});
});

//the paper functions
function Pager(itemsPerPage) {
    this.itemsPerPage = itemsPerPage;
    this.currentPage = 1;
    this.pages = 0;
    this.inited = false;
    
    this.showRecords = function(from, to) {     
        var rows = i;
		for (var rand = 1; rand <= rows; rand++)
		{
			if($(".custQ").eq(rand-1).attr("data-sectype") <= r && $(".custQ").eq(rand-1).attr("data-sectype") > 0 )
			{
				$("#sec"+ $(".custQ").eq(rand-1).attr("data-sectype")).hide();
			}
		}
        // i starts from 1 to skip table header row
        for (var k = 1; k <= rows; k++) {
            if (k >= from && k <= to)
			{	
				$("#sec"+ $(".custQ").eq(k-1).attr("data-sectype")).show();
				$(".custQ").eq(k-1).show();
			}	
            else
			{
                $(".custQ").eq(k-1).hide();
			} 
        }
    }
    
   this.showPage = function(pageNumber) {
    	if (!this.inited) {
    		return;
    	}
        var oldPageAnchor = document.getElementById('pg'+this.currentPage);
        oldPageAnchor.className = 'pg-normal';
        
        this.currentPage = pageNumber;
        var newPageAnchor = document.getElementById('pg'+this.currentPage);
        newPageAnchor.className = 'pg-selected';
        
        var from = (pageNumber - 1) * this.itemsPerPage + 1;
        var to = from + this.itemsPerPage - 1;
        this.showRecords(from, to);
    }   
    
    this.prev = function() {
        if (this.currentPage > 1)
		{
            this.showPage(this.currentPage - 1);
		}
    }
    
    this.next = function() {
        if (this.currentPage < this.pages) {
            this.showPage(this.currentPage + 1);
        }
    }                        
    
    this.init = function() {
        var rows = i;
        var records = rows; 
        this.pages = Math.ceil(records / this.itemsPerPage);
        this.inited = true;
    }

    this.showPageNav = function(pagerName, positionId) {
    	if (!this.inited) {
			return;
    	}
		var element = document.getElementById(positionId);
    	var pagerHtml = '<span onclick="' + pagerName + '.prev();" class="pg-normal"> &#171 Prev </span> | ';
        for (var page = 1; page <= this.pages; page++)
		{
            pagerHtml += '<span id="pg' + page + '" class="pg-normal" onclick="' + pagerName + '.showPage(' + page + ');">' + page + '</span> | ';
		}
        pagerHtml += '<span onclick="'+pagerName+'.next();" class="pg-normal"> Next &#187;</span>';            
        
        element.innerHTML = pagerHtml;
    }
}


$(window).load(function(){   
		var $window = $(window),
       $stickyEl = $('#sideBar'),
       elTop = $stickyEl.position().top;
	   	var footerYLoc = $('#footer').position().top;
	//	$stickyEl.toggleClass('sticky');
	  // var elBottom = elTop + 500;

   $window.scroll(function() {
		var scrollBottom = $(window).scrollTop() + $(window).height();
       $stickyEl.toggleClass('sticky', $window.scrollTop() > elTop + 75);
	//	alert(scrollBottom - 40);
		// alert($window.scrollTop());

    });
	


});

	
